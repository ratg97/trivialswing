# **Proyecto NetBeans** #

El juego del **'Trivial'** para el idioma *Español*.

![Captura3.PNG](https://bitbucket.org/repo/ekkrx7A/images/2629802790-Captura3.PNG)

# **Metodología:** #

1. Swing

1. MVC

1. Constantes

1. Expresiones regulares

1. Ficheros de texto

1. Ficheros de acceso aleatorio