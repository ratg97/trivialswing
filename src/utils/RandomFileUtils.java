package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import static java.lang.System.exit;
import java.util.ArrayList;
import model.Question;

public class RandomFileUtils {

    private RandomFileUtils() {
    }

    public static void createFilesFirstTime(ArrayList<Question> questions) {
        RandomAccessFile fileQuestions = null;
        RandomAccessFile fileAnswers = null;
        try {
            fileQuestions = new RandomAccessFile(Constants.QUESTIONS_RANDOM_FILE, "rw");
            fileAnswers = new RandomAccessFile(Constants.ANSWERS_RANDOM_FILE, "rw");
            StringBuffer buffer = null;
            for (int i = 0; i < questions.size(); i++) {
                buffer = new StringBuffer(questions.get(i).getQuestion());
                buffer.setLength(Constants.LENGTH_MAX_QUESTION_ANSWER);
                fileQuestions.writeChars(buffer.toString());
                for (int j = 0; j < questions.get(i).getAnswers().length; j++) {
                    buffer = new StringBuffer(questions.get(i).getAnswers()[j]);
                    buffer.setLength(Constants.LENGTH_MAX_QUESTION_ANSWER);
                    fileQuestions.writeChars(buffer.toString());
                }
                buffer = new StringBuffer(questions.get(i).getSolution());
                buffer.setLength(Constants.LENGTH_MAX_QUESTION_ANSWER);
                fileAnswers.writeChars(buffer.toString());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("--> No borres o edites ningún fichero durante la ejecución");
            exit(0);
        } catch (IOException ex) {
            System.out.println("--> Error de e/s");
        } finally {
            try {
                if (fileQuestions != null) {
                    fileQuestions.close();
                }
                if (fileAnswers != null) {
                    fileAnswers.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static Question readQuestionRandomFile() {
        RandomAccessFile fileQuestions = null;
        RandomAccessFile fileAnswers = null;
        Question q = null;
        try {
            fileQuestions = new RandomAccessFile(Constants.QUESTIONS_RANDOM_FILE, "r");
            fileAnswers = new RandomAccessFile(Constants.ANSWERS_RANDOM_FILE, "r");
            int posQuestion = Constants.SIZE_QUESTION_ANSWER * Constants.DISTANCE_BETWEEN_QUESTION * ((int) ((Math.random() * getNQuestions()) + 1) - 1);
            int posAnswer = posQuestion / Constants.DISTANCE_BETWEEN_QUESTION;
            if (fileQuestions.length() > 0 && posQuestion < fileQuestions.length()) {
                q = new Question();
                //Nombre de la pregunta y respuestas
                char code[] = new char[Constants.LENGTH_MAX_QUESTION_ANSWER];
                char aux;
                fileQuestions.seek(posQuestion);
                for (int i = 0; i < code.length; i++) {
                    aux = fileQuestions.readChar();
                    code[i] = aux;
                }
                q.setQuestion(new String(code).trim());
                //System.out.println(q.getQuestion());
                for (int i = 0; i < 3; i++) {
                    posQuestion = posQuestion + Constants.SIZE_QUESTION_ANSWER;
                    fileQuestions.seek(posQuestion);
                    for (int j = 0; j < code.length; j++) {
                        aux = fileQuestions.readChar();
                        code[j] = aux;
                    }
                    q.setAnswers(new String(code).trim(), i);
                }
                //Solución
                fileAnswers.seek(posAnswer);
                for (int i = 0; i < code.length; i++) {
                    aux = fileAnswers.readChar();
                    code[i] = aux;
                }
                q.setSolution(new String(code).trim());
                // System.out.println(q.getSolution());
                fileAnswers.close();
                fileQuestions.close();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("--> No borres o edites ningún fichero durante la ejecución");
            exit(0);
        } catch (IOException ex) {
            System.out.println("--> Error de e/s");
        } finally {
            try {
                if (fileQuestions != null) {
                    fileQuestions.close();
                }
                if (fileAnswers != null) {
                    fileAnswers.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return q;
    }

    public static int getNQuestions() throws IOException {
        RandomAccessFile fileQuestions = new RandomAccessFile(Constants.QUESTIONS_RANDOM_FILE, "r");
        return (int) (fileQuestions.length() / (Constants.SIZE_QUESTION_ANSWER * 4));
    }

    public static int getNAnswers() throws IOException {
        RandomAccessFile fileAnswers = new RandomAccessFile(Constants.ANSWERS_RANDOM_FILE, "r");
        return (int) (fileAnswers.length() / (Constants.SIZE_QUESTION_ANSWER));
    }

    public static boolean isNQuestionsEqualsNAnswers() throws IOException {
        return getNQuestions() == getNQuestions();
    }
}
