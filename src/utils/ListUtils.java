package utils;

import java.io.IOException;
import java.util.ArrayList;
import model.Question;

public class ListUtils {
    
    public static ArrayList<Question> questionsFromRandomFile;
    
    public static void setQuestionsGame() throws IOException {
        questionsFromRandomFile = new ArrayList<Question>();
        if (RandomFileUtils.isNQuestionsEqualsNAnswers()) {
            //Si hay más de 10 pues se escogen 10, sino las que haya
            while (questionsFromRandomFile.size() != (RandomFileUtils.getNQuestions() >= Constants.NQUESTION_GAME ? Constants.NQUESTION_GAME : RandomFileUtils.getNQuestions())) {
                Question q = RandomFileUtils.readQuestionRandomFile();
                if (!checkQuestionRepeat(questionsFromRandomFile, q)) {
                    questionsFromRandomFile.add(q);
                }
            }
        }
    }
    
    private static boolean checkQuestionRepeat(ArrayList<Question> questionsFromRandomFile, Question q) {
        for (Question que : questionsFromRandomFile) {
            if (que.getQuestion().equalsIgnoreCase(q.getQuestion())) {
                return true;
            }
        }
        return false;
    }
}
