package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import model.Question;


public class TextFileUtils {

    private TextFileUtils() {
    }

    public static void readTextFile(ArrayList<Question> questions) {
        FileReader file = null;
        BufferedReader read = null;
        try {
            file = new FileReader(Constants.TEXT_FILE);
            read = new BufferedReader(file);
            ArrayList<String> questionsString = new ArrayList<String>();
            String line;
            int i = 0;
            while ((line = read.readLine()) != null) {
                if (i >= 0 && i <= Constants.DISTANCE_BETWEEN_QUESTION) {
                    questionsString.add(line.trim());
                }
                if (i == Constants.DISTANCE_BETWEEN_QUESTION) {
                    Question q = new Question();
                    q.setQuestion(questionsString.get(0));
                    for (int j = 0; j < 3; j++) {
                        q.setAnswers(questionsString.get(j + 1).trim(), j);
                    }
                    q.setSolution(questionsString.get(questionsString.size() - 1).trim());
                    if (isClonQuestion(questions, q)) {
                        System.out.println("--> Fichero de texto erróneo\n--> Preguntas y soluciones iguales...\n--> Una de ellas es: " + q.getQuestion());
                        exit(0);
                    }
                    if (isAnyAnswerSolution(q)) {
                        questions.add(q);
                    } else {
                        System.out.println("--> Fichero de texto erróneo\n--> Las respuestas posibles no coinciden con la solución\n--> En la pregunta: " + q.getQuestion());
                        exit(0);
                    }
                    i = 0;
                    questionsString.clear();
                } else {
                    i++;
                }
            }
            read.close();
        } catch (FileNotFoundException io) {
            System.out.println("--> Fichero por defecto no encontrado");
            exit(0);
        } catch (IOException en) {
            System.out.println("--> Error e/s");
        } finally {
            try {
                if (read != null) {
                    read.close();
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }

    private static boolean isAnyAnswerSolution(Question q) {
        int len = q.getAnswers().length;
        for (int i = 0; i < len; i++) {
            if (q.getAnswers()[i].equals(q.getSolution())) {
                return true;
            }
        }
        return false;
    }
//corregir

    private static boolean isClonQuestion(ArrayList<Question> questionsString, Question q) {
        int len = questionsString.size();
        for (int i = 0; i < len; i++) {
            if (q.getQuestion().equalsIgnoreCase(questionsString.get(i).getQuestion())) {
                if (q.getSolution().equalsIgnoreCase(questionsString.get(i).getSolution())) {
                    return true;
                }
            }
        }
        return false;
    }
}
