package utils;

import java.io.File;
import javax.swing.ImageIcon;

public class Constants {

    private Constants() {
    }
    
    public static final String GAME_NAME = "TRIVIAL";

    public static final int LOOP_VALUE = -1;

    public static final String PLAY_NAME = "Jugar";

    public static final int PLAY_VALUE = 1;

    public static final String INSTRUCTIONS_NAME = "Instrucciones";

    public static final int INSTRUCTIONS_VALUE = 2;

    public static final String EXIT_NAME = "Salir";

    public static final int EXIT_VALUE = 0;

    public static final File TEXT_FILE = new File("preguntas.txt");

    public static final File QUESTIONS_RANDOM_FILE = new File("preguntas.dat");

    public static final File ANSWERS_RANDOM_FILE = new File("respuestas.dat");

    public static final int LENGTH_MAX_QUESTION_ANSWER = 100;

    public static final int SIZE_QUESTION_ANSWER = (LENGTH_MAX_QUESTION_ANSWER * 2);

    public static final int NQUESTION_GAME = 10;

    public static final int DISTANCE_BETWEEN_QUESTION = 4;

    public static final String REGEX_ANSWER_GAME = "[123]{1,1}";

    public static final String REPEAT_NAME = "Repetir";

    public static final int REPEAT_VALUE = 1;

    public static final String NO_REPEAT_NAME = "Regresar al Menú";

    public static final int NO_REPEAT_VALUE = 0;
    
}
