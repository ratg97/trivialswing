package controller;

import java.io.IOException;
import model.Player;
import model.Question;
import utils.ListUtils;
import view.GameView;

public class GameController {

    private static final int RIGHT_POINTS = 1;
    private static final int WRONG_POINTS = -1;

    private GameView gameView;
    private Question q;
    private Player p;

    public GameController(GameView gameView, Question q, Player p) {
        this.gameView = gameView;
        this.q = q;
        this.p = p;
    }

    private boolean checkAnswer(String answerButton) {
        return this.q.getSolution().equalsIgnoreCase(answerButton);
    }

    public int getUpdatePoints(String answerButton) {
        return p.getPoints() + (checkAnswer(answerButton) ? RIGHT_POINTS : WRONG_POINTS);
    }

    public void setUpdatePoints(String pointsLabel) {
        this.p.setPoints(Integer.parseInt(pointsLabel));
    }

    public void setQuestion(int nQuestion) {
        this.q.setQuestion(ListUtils.questionsFromRandomFile.get(nQuestion).getQuestion());
        this.q.setAnswers(ListUtils.questionsFromRandomFile.get(nQuestion).getAnswers()[0], 0);
        this.q.setAnswers(ListUtils.questionsFromRandomFile.get(nQuestion).getAnswers()[1], 1);
        this.q.setAnswers(ListUtils.questionsFromRandomFile.get(nQuestion).getAnswers()[2], 2);
        this.q.setSolution(ListUtils.questionsFromRandomFile.get(nQuestion).getSolution());
    }

    public void updateQuestion() {
        this.gameView.updateView();
    }

    public void exit() {
        this.gameView.dispose();
    }

    public String showRepeat() throws IOException {
        return "Puntos obtenidos: " + this.p.getPoints() + "\n\n¿Quieres repetir?";
    }

    public void updateList() throws IOException {
        ListUtils.questionsFromRandomFile.clear();
        ListUtils.setQuestionsGame();
    }
}
