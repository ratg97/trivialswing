/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import utils.ListUtils;
import view.GameView;
import view.MenuView;

/**
 *
 * @author ratg9
 */
public class MenuController {

    private MenuView menuView;

    public MenuController(MenuView menuView) {
        this.menuView = menuView;
    }

    public void play() {
        this.menuView.dispose();
        new GameView().setVisible(true);
    }

    public boolean checkSettings() throws IOException {
        ListUtils.setQuestionsGame();
        return !ListUtils.questionsFromRandomFile.isEmpty();
    }

    public String showInformation(){
        return "Este trivial consiste en que el jugador tendrá que:\nen una pregunta escoger entre 3 respuestas, la correcta.\nSaldrán 10 preguntas siempre a menos que no hubiera tantas.";
    }
    
    public String showError() {
        return "Hay problemas con la lista cargada.";
    }

    public String showTitleError() {
        return "Problemas encontrados.";
    }

    public void exit() {
        this.menuView.dispose();
    }

    public String showExit() {
        return "Gracias por jugar.";
    }
}
