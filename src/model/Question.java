package model;

public class Question {

    private String question;
    private String[] answers;
    private String solution;

    public Question(String question) {
        this.question = question;
    }

    public Question() {
        this.answers = new String[3];
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswers(String answer, int i) {
        this.answers[i] = answer;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public String getSolution() {
        return solution;
    }
}
